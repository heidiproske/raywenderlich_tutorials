//
//  ViewController.swift
//  BullsEye
//
//  Created by Heidi Proske on 2/25/15.
//  Copyright (c) 2015 Heidi Proske. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetValueLabel: UILabel!
    @IBOutlet weak var roundValueLabel :UILabel!
    @IBOutlet weak var scoreValueLabel: UILabel!
    
    var currentValue = 0
    var score: Int = 0 {
        didSet {
            scoreValueLabel.text = String(score)
        }
    }
    
    var roundNumber: Int = 0 {
        didSet {
            roundValueLabel.text = String(roundNumber)
        }
    }
    var targetValue: Int = 0 {
        didSet {
            targetValueLabel.text = String(targetValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeSlider()
        startNewGame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showAlert() {
        
        let difference = abs(targetValue - currentValue)
        var points = 100 - difference
        
        let msg = "You scored \(points) points"
        var title: String
        
        if difference == 0 {
            title = "Perfect!"
            points += 100 // give user bonus score for getting it perfect
        } else if difference < 5 {
            title = "You almost had it!"
            if difference == 1 {
                points += 50 // bonus for being so close!
            }
        } else if difference < 10 {
            title = "Pretty good!"
        } else {
            title = "Not even close..."
        }
        
        score += points
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { action in
            self.startNewRound()
        }
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderMoved(slider: UISlider) {
        currentValue = lroundf(slider.value)
    }
    
    @IBAction func startOver() {
        startNewGame()
        
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        view.layer.addAnimation(transition, forKey: nil)
    }
    
    func startNewGame() {
        roundNumber = 0
        score = 0
        startNewRound()
    }
    
    func startNewRound() {
        roundNumber++
        resetSlider()
        generateNewTarget()
    }
    
    func resetSlider() {
        currentValue = 50
        slider.value = Float(currentValue)
    }
    
    func generateNewTarget() {
        var newTargetValue = targetValue
        while newTargetValue == targetValue {
            newTargetValue = 1 + Int(arc4random_uniform(100)) // to ensure that we convert [0..100) to [1..100]
        }
        targetValue = newTargetValue
    }
    
    func customizeSlider() {
        let thumbImgNormal = UIImage(named: "SliderThumb-Normal")
        let thumbImgHighlighted = UIImage(named: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImgNormal, forState: UIControlState.Normal)
        slider.setThumbImage(thumbImgHighlighted, forState: UIControlState.Highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        if let trackLeftImg = UIImage(named: "SliderTrackLeft") {
            let trackLeftResizable = trackLeftImg.resizableImageWithCapInsets(insets)
            slider.setMinimumTrackImage(trackLeftResizable, forState: UIControlState.Normal)
        }
        let trackRightImg = UIImage(named: "SliderTrackRight")
        let trackRightResizable = trackRightImg?.resizableImageWithCapInsets(insets)
        slider.setMaximumTrackImage(trackRightResizable, forState: .Normal)
    }
}

