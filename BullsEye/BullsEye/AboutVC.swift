//
//  AboutVC.swift
//  BullsEye
//
//  Created by Heidi Proske on 2/26/15.
//  Copyright (c) 2015 Heidi Proske. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    
    @IBOutlet weak var webview: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let htmlFile = NSBundle.mainBundle().pathForResource("BullsEye", ofType: "html") {
            let htmlData = NSData(contentsOfFile: htmlFile)
            let baseUrl = NSURL.fileURLWithPath(NSBundle.mainBundle().bundlePath)
            webview.loadData(htmlData, MIMEType: "text/html", textEncodingName: "UTF-8", baseURL: baseUrl)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
